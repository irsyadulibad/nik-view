<?php

use Irsyadulibad\NIKValidator\Validator;

function index()
{
    return view('home');
}

function check()
{
    if (validation__run('nik')) {
        $nik = input_post('nik');
        $nik = Validator::set($nik)->parse();

        return view('nik', compact('nik'));
    } else {
        $errors = validation__errors();
        return view('home', compact('errors'));
    }
}

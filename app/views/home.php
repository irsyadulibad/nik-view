<?= __extend('layouts/app') ?>

<?= __section('content') ?>
<div class="row justify-content-center">
    <div class="col-md-5">
        <div class="card mt-4">
            <div class="card-body">
                <form action="" method="post">
                    <?= csrf_field() ?>
                    <div class="mb-3 text-center">
                        <h2>NIK Validator</h2>
                    </div>
                    <div class="mb-3">
                        <label for="nik">NIK</label>
                        <input type="text" class="form-control <?= ($errors['nik'] ?? null) ? 'is-invalid' : '' ?>" name="nik" id="nik" autofocus>
                        <div class="invalid-feedback">
                            <?= $errors['nik'] ?? '' ?>
                        </div>
                    </div>
                    <div class="mb-3 text-center">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?= __endSection() ?>

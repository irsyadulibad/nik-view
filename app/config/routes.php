<?php

$config['auto_route'] = false;
$config['default_controller'] = 'home';

route__get('/', 'home@index');
route__post('/', 'home@check');

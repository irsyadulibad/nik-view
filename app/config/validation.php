<?php

$validation = [
    'login' => [
        'username' => [
            'label' => 'User Name',
            'rules' => 'required|min_length[4]'
        ],
        'password' => 'required'
    ],
    'nik' => [
        'nik' => [
            'label' => 'NIK',
            'rules' => 'required|min_length[16]'
        ]
    ]
];

return $validation;

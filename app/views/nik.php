<?= __extend('layouts/app') ?>

<?= __section('content') ?>
<div class="row justify-content-center">
    <div class="col-md-6">
        <?php if ($nik->valid) : ?>
            <div class="card mt-4">
                <div class="card-header text-center">
                    <h5>NIK: <?= $nik->nik ?></h5>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <tr>
                                <td>Kode Unik</td>
                                <td><?= $nik->uniqueCode ?></td>
                            </tr>
                            <tr>
                                <td>Gender</td>
                                <td><?= $nik->gender ?></td>
                            </tr>
                            <tr>
                                <td>Tanggal Lahir</td>
                                <td><?= $nik->born->full ?></td>
                            </tr>
                            <tr>
                                <td>Zodiak</td>
                                <td><?= $nik->zodiac ?></td>
                            </tr>
                            <tr>
                                <td>Umur</td>
                                <td><?= $nik->age->year ?></td>
                            </tr>
                            <tr>
                                <td>Alamat</td>
                                <td>
                                    <?= $nik->address->subDistrict ?>
                                    <?= $nik->address->city ?>
                                    <?= $nik->address->province ?>
                                </td>
                            </tr>
                            <tr>
                                <td>Kode Pos</td>
                                <td><?= $nik->postalCode ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="card-footer text-center">
                    <a href="/" class="btn btn-sm btn-info text-white">Kembali</a>
                </div>
            </div>
        <?php else : ?>
            <div class="card mt-4">
                <div class="card-body bg-danger text-white text-center">
                    NIK yang anda masukkan tidak valid
                </div>
                <div class="card-footer text-start">
                    <a href="/" class="btn btn-sm btn-info text-white">Kembali</a>
                </div>
            </div>
        <?php endif ?>
    </div>
</div>
<?= __endSection() ?>
